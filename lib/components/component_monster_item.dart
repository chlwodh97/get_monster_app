import 'package:flutter/material.dart';
import 'package:get_monster_app/model/monster_Item.dart';

class ComponentMonsterItem extends StatefulWidget {
  const ComponentMonsterItem({
    super.key,
    required this.monsterItem,
    required this.callback,

  });


  final MonsterItem monsterItem;
  final Voidcallback callback;


  @override
  State<ComponentMonsterItem> createState() => _ComponentMonsterItemState();
}

class _ComponentMonsterItemState extends State<ComponentMonsterItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(),
          )
        ],
      ),
    );
  }
}
