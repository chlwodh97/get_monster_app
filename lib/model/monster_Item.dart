class MonsterItem {
  String photoUrl;
  String name;
  num hp;
  num speed;
  num hitPower;
  num defPower;

  MonsterItem(this.photoUrl, this.name, this.hp, this.speed, this.hitPower, this.defPower);
}